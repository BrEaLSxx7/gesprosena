(() => {
	'use strict';

	angular.module('GesPro').constant('host', 'http://localhost/gesprosena/');

	angular
		.module('GesPro')
		.config(config);

	config.$inject = ['$httpProvider', '$stateProvider', '$urlRouterProvider'];
	function config($httpProvider, $stateProvider, $urlRouterProvider) {
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$httpProvider.defaults.headers.delete = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' };
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('inicio', {
				url: '/',
				controller: 'inicioController',
				templateUrl: 'view/inicio.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load([{
							files: ['controller/inicioController.js']
						}]);
					}]
				}
			})
			.state('dashboard', {
				url: '/dashboard',
				controller: 'dashboardController',
				templateUrl: 'view/dashboard.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load([{
							files: ['controller/dashboardController.js']
						}]);
					}]
				}
			})
			.state('tipoproyecto', {
				url: '/tipoproyecto',
				controller: 'tipoproyectoController',
				templateUrl: 'view/tipoproyecto.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load([{
							files: ['controller/tipoproyectoController.js']
						}]);
					}]
				}
			})
			.state('crearproyecto', {
				url: '/crearproyecto',
				controller: 'crearproyectoController',
				templateUrl: 'view/crearproyecto.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load([{
							files: ['controller/crearproyectoController.js']
						}]);
					}]
				}
			})
			.state('nuevotipo', {
				url: '/nuevotipoproyecto',
				controller: 'nuevotipoController',
				templateUrl: 'view/nuevotipo.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load([{
							files: ['controller/nuevotipoController.js']
						}]);
					}]
				}
			});
	}

})();