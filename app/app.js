(() => {
	'use strict';

	angular.module('GesPro', [
		'ui.router',
		'oc.lazyLoad',
		'ngFileUpload',
		'ngStorage',
		'ngImgCrop',
		'ngMaterial',
		'ngAnimate',
		'ngAria',
		'ngMessages',
		'ngStorage'
	]);
})();