(() => {
	'use strict';
	angular
		.module('GesPro')
		.controller('crearproyectoController', crearproyectoController);
	crearproyectoController.$inject = ['$scope', 'DataBaseService', '$log', '$state', 'Upload', 'host', '$timeout', '$sessionStorage'];
	function crearproyectoController($scope, DataBase, $log, $state, Upload, host, $timeout, $sessionStorage) {
		$scope.gestionrol = false;
		function ponerplantillahtml(plantilla) {
			return plantilla;
		}
		// $scope.crud = ['Desactivar', 'Modificar', 'Eliminar'];

		if (typeof ($sessionStorage.date) !== "undefined") {
			$scope.date = $sessionStorage.date;
			$scope.name = $scope.date.nombre;
			$scope.correo = $scope.date.usuario;
			$scope.img = $scope.date.image + "?" + Date.now();
			$scope.tipocrear = ((type) => {
				DataBase.tipoproyecto(type)
					.then((response) => {
						$scope.plantilla = response.data[0];
						$scope.campos = response.data[1];
						$log.log(response.data);
						$scope.plantilla.map((e, i) => {
							for (var j = 0; j < $scope.campos.length; j++) {
								if (e.tipo_campo_id === $scope.campos[j].id) {
									$log.log($scope.campos[j].nombre);
									e.nombre = $scope.campos[j].nombre;
								}
							}
						});
						$scope.tipodatos = ponerplantillahtml($scope.plantilla);

					})
					.catch((error) => {
						$log.error(error);
					});
			});
			$scope.upload = function (dataUrl, name) {
				Upload.upload({
					url: host + 'controller/uploadimg.controller.php',
					data: {
						file: Upload.dataUrltoBlob(dataUrl, name)
					},
				})
					.then((response) => {
						$scope.result = response.data;
						$scope.img = $scope.result.resultado;
						$scope.date.image = $scope.result.resultado;
						$scope.img = $scope.date.image + "?" + Date.now();
					})
					.catch(function (response) {
						if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
					});
			};
			$scope.logout = () => {
				delete $sessionStorage.date;
				$scope.out = true;
				DataBase.logout($scope.out)
					.then((response) => {
						if (response.data.respuesta) {
							toastr.success(response.data.mensaje);
							$state.go('inicio');
						}
					})
					.catch((error) => {
						$log.error(error);
					});
			};
			DataBase.validaterol($scope.date.rol_id)
				.then((response) => {
					$scope.gestionrol = response.data.rol;
					sessionStorage.gestion= $scope.gestionrol;
				})
				.catch((error) => {
					$log.error(error);
				});

		} else {
			$state.go('inicio');
		}
	}
})();