(() => {
	'use strict';
	angular
		.module('GesPro')
		.controller('dashboardController', dashboardController);
	dashboardController.$inject = ['$scope', 'DataBaseService', '$log', '$state', 'Upload', 'host', '$timeout', '$sessionStorage'];
	function dashboardController($scope, DataBase, $log, $state, Upload, host, $timeout, $sessionStorage) {
		$scope.gestionrol = false;
		$scope.sennova = 0;
		//$scope.date = $sessionStorage.date;
		if (typeof ($sessionStorage.date) !== "undefined") {
			$scope.date = $sessionStorage.date;
			DataBase.validaterol($scope.date.rol_id)
				.then((response) => {
					$scope.gestionrol = response.data.rol;
					$sessionStorage.gestion = $scope.gestionrol;
					$scope.name = $scope.date.nombre;
					$scope.correo = $scope.date.usuario;
					$scope.img = $scope.date.image + "?" + Date.now();
				})
				.catch((error) => {
					$log.error(error);
				});
			$scope.val = $sessionStorage.gestion;
			if ($scope.val) {
				$scope.datatotales = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
				$scope.datatotal = [];
				$log.log($scope.date.tipoproyecto);
				for (let i = 0; i < $scope.date.tipoproyecto.length; i++) {
					$log.log($scope.date.tipoproyecto[i].id);
					$scope.datatotal.push({ id: $scope.date.tipoproyecto[i].id, total: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], nombre: $scope.date.tipoproyecto[i].nombre });
				}
				$scope.$watch('anoselect', (newValue, oldValue) => {
					if (newValue === oldValue) {
						return;
					}
					scope.datatotal.map((e) => {
						for (var i = 0; i < $scope.date.proyectos.length; i++) {
							if (e.id === $scope.date.proyectos[i].id) {
								if ($scope.date.proyectos[i].ano === $scope.anoselect) {
									for (var j = 0; j < $scope.datatotal.length; j++) {
										if (j === $scope.date.proyectos[i].mes) {
											$scope.datatotal[j].total = $scope.datatotal[j].total + 1;
										}
									}
								}
							}
						}
					});
				});
				if ($scope.date.proyectos.length === 0) {
					$scope.temp = new Date();
					$scope.anomax = $scope.temp.getFullYear();
					$scope.anoselect = $scope.anomax;
					$scope.fechaarray = [$scope.anoselect];
				} else {
					$scope.fechaarray = [];
					$scope.date.proyectos.map((e) => {
						e.ano = parseInt(e.created_at.slice(0, 4));
						e.mes = parseInt(e.created_at.slice(5, 7));

					});
					$scope.anomin = $scope.date.proyectos[0].created_at;
					$scope.anomin = parseInt($scope.anomin.slice(0, 4));
					$scope.temp = new Date();
					$scope.anomax = $scope.temp.getFullYear();
					$scope.anoselect = $scope.anomax;
					$scope.sennova = 0;
					$scope.formativos = 0;
					$scope.investigacion = 0;
					$scope.datatotal.map((e) => {
						for (var i = 0; i < $scope.date.proyectos.length; i++) {
							if (e.id === $scope.date.proyectos[i].tipo_proyecto_id) {
								if ($scope.date.proyectos[i].ano === $scope.anoselect) {
									for (var j = 0; j < 12; j++) {
										if (j === ($scope.date.proyectos[i].mes - 1)) {
											e.total[j] = e.total[j] + 1;
										}
									}
								}
							}
						}
					});
				}
				$scope.total = 0;
				for (var i = 0; i < $scope.datatotal.length; i++) {
					for (var j = 0; j < 12; j++) {
						$scope.total = $scope.total + $scope.datatotal[i].total[j];
					}
				}
				for (var i = $scope.anomin; i <= $scope.anomax; i++) {
					$scope.fechaarray.push(i);
				}
			}
			$log.log($scope.datatotal);
			$scope.date.tipoproyecto.map((e) => {
				e.total = 0;
				for (var i = 0; i < $scope.datatotal.length; i++){
					if (e.id = $scope.datatotal[i].id) {
						for (var j = 0; j < 12; j++){
							e.total = e.total + $scope.datatotal[i].total[j];
						}
					}
				}
			});
			$scope.upload = function (dataUrl, name) {
				Upload.upload({
					url: host + 'controller/uploadimg.controller.php',
					data: {
						file: Upload.dataUrltoBlob(dataUrl, name)
					},
				})
					.then((response) => {
						$scope.result = response.data;
						$scope.img = $scope.result.resultado;
						$scope.date.image = $scope.result.resultado;
						$scope.img = $scope.date.image + "?" + Date.now();
					})
					.catch(function (response) {
						if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
					});
			};
			$scope.logout = () => {
				delete $sessionStorage.date;
				$scope.out = true;
				DataBase.logout($scope.out)
					.then((response) => {
						if (response.data.respuesta) {
							toastr.success(response.data.mensaje);
						}
					})
					.catch((error) => {
						$log.error(error);
					});
				$state.go('inicio');
			};
			$scope.valrol = () => {
				DataBase.validaterol($scope.date.rol_id)
					.then((response) => {
						if (response.data.rol) {
							$scope.gestionrol = response.data.rol;
						} else {
							toastr.error('No tiene permisos para realizar esta acción');
							$state.go('dashboard');
						}
					})
					.catch((error) => {
						$log.error(error);
					});
			};
			var ctx = document.getElementById("myChart").getContext("2d");
			var myChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ["Enero", "Febero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					datasets: [{
						label: '# de proyectos aprobados',
						data: $scope.datatotales,
						backgroundColor: [
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)',
							'rgba(255, 99, 132, 0.6)'
						],
						borderColor: [
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)',
							'rgba(255,99,132,1)'
						],
						borderWidth: 1
					},
					{
						label: '# de ' + $scope.datatotal[0].nombre,
						data: $scope.datatotal[0].total,
						backgroundColor: [
							'rgba(54, 162, 235,0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235,0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)',
							'rgba(54, 162, 235, 0.6)'
						],
						borderColor: [
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(54, 162, 235, 1)'
						],
						borderWidth: 1
					},
					{
						label: '# de ' + $scope.datatotal[1].nombre,
						data: $scope.datatotal[1].total,
						backgroundColor: [
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
							'rgba(255, 206, 86, 0.6)',
						],
						borderColor: [
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(255, 206, 86, 1)',
						],
						borderWidth: 1
					},
					{
						label: '# de ' + $scope.datatotal[2].nombre,
						data: $scope.datatotal[2].total,
						backgroundColor: [
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)',
							'rgba(139,195,74, 0.6)'
						],
						borderColor: [
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)',
							'rgba(139,195,74, 1)'
						],
						borderWidth: 1
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true,
								responsive: true
							}
						}]
					}
				}
			});
		} else {
			$state.go('inicio');
		}
	}

})();