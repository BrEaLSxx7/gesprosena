(() => {
	'use strict';
	angular
		.module('GesPro')
		.controller('inicioController', inicioController);
	inicioController.$inject = ['$scope', 'DataBaseService', '$log', '$state', '$sessionStorage'];
	function inicioController($scope, DataBase, $log, $state, $sessionStorage) {
		if (typeof ($sessionStorage.date) === "undefined") {
			$scope.determinate = false;
			$scope.user = {};
			$scope.registro = {};
			$scope.date = {};
			$scope.correo;
			$scope.recoverpassword = () => {
				$log.log($scope.correo);
				DataBase.recuperar($scope.correo)
					.then((response) => {
						if (response.data.respuesta) {
							toastr.success(response.data.mensaje);
						} else {
							toastr.error(response.data.mensaje);
						}
					})
					.catch((error) => {
						$log.error(error);
					});
			};
			$scope.login = () => {
				$scope.determinate = true;
				DataBase.iniciarsesion($scope.user)
					.then((response) => {
						if (response.data.respuesta) {
							toastr.success(response.data.mensaje);
							$scope.determinate = false;
							$scope.date = response.data.resultado;
							$sessionStorage.date = $scope.date;
							$state.go('dashboard');
						} else {
							toastr.error(response.data.mensaje);
							$scope.determinate = false;
						}
					})
					.catch((error) => {
						$log.error(error);
					});
			};
			$scope.register = () => {
				$scope.determinate = true;
				setTimeout(() => {
					if ($scope.registro.correo === $scope.registro.verificarcorreo) {
						if ($scope.registro.password === $scope.registro.confirmarpassword) {
							$scope.registeruser = {
								perfil: $scope.registro.perfil,
								correo: $scope.registro.correo,
								tipodocumento: $scope.registro.TipoDocumento,
								password: $scope.registro.password,
								numerodocumento: $scope.registro.numeroDocumento,
								nombre: $scope.registro.nombre
							};
							DataBase.regitrarusuario($scope.registeruser)
								.then((response) => {
									$log.log(response.data.respuesta);
									if (response.data.respuesta) {
										toastr.success(response.data.mensaje);
										$scope.determinate = false;
										$scope.registro = {};
									} else {
										toastr.error(response.data.mensaje);
										$scope.determinate = false;
									}
								})
								.catch((error) => {
									$log.error(error);
								});
						} else {
							toastr.error('las contraseñass deben ser iguales');
							$scope.determinate = false;
						}
					} else {
						toastr.error('los correos deben ser iguales');
						$scope.determinate = false;
					}
				}, 1500);
			};
		}else {
			$state.go('dashboard');
		}
	}
})();