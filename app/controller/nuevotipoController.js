(() => {
	'use strict';
	angular
		.module('GesPro')
		.controller('nuevotipoController', nuevotipoController);
	nuevotipoController.$inject = ['$scope', 'DataBaseService', '$log', '$state', 'Upload', 'host', '$timeout', '$sessionStorage'];
	function nuevotipoController($scope, DataBase, $log, $state, Upload, host, $timeout, $sessionStorage) {
		$scope.gestionrol = true;
		$scope.date = $sessionStorage.date;
		if (typeof ($sessionStorage.date) === "undefined") {
			DataBase.validaterol($scope.date.rol_id)
				.then((response) => {
					if (response.data.rol) {
						$scope.gestionrol = response.data.rol;
					} else {
						toastr.error('No tiene permisos para realizar esta acción');
						$state.go('dashboard');
					}
				})
				.catch((error) => {
					$log.error(error);
				});
			$scope.valrol = () => {
				DataBase.validaterol($scope.date.rol_id)
					.then((response) => {
						if (response.data.rol) {
							$scope.gestionrol = response.data.rol;
						} else {
							toastr.error('No tiene permisos para realizar esta acción');
							$state.go('dashboard');
						}
					})
					.catch((error) => {
						$log.error(error);
					});
			};
			$scope.name = $scope.date.nombre;
			$scope.correo = $scope.date.usuario;
			$scope.img = $scope.date.image + "?" + Date.now();
			$scope.upload = function (dataUrl, name) {
				Upload.upload({
					url: host + 'controller/uploadimg.controller.php',
					data: {
						file: Upload.dataUrltoBlob(dataUrl, name)
					},
				})
					.then((response) => {
						$scope.result = response.data;
						$scope.img = $scope.result.resultado;
						$scope.date.image = $scope.result.resultado;
						$scope.img = $scope.date.image + "?" + Date.now();
					})
					.catch(function (response) {
						if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
					});
			};
			$scope.logout = () => {
				sessionStorage.clear();
				$scope.out = true;
				DataBase.logout($scope.out)
					.then((response) => {
						if (response.data.respuesta) {
							toastr.success(response.data.mensaje);
							$state.go('inicio');
						}
					})
					.catch((error) => {
						$log.error(error);
					});
			};
		} else {
			$state.go('inicio');
		}
	}
})();