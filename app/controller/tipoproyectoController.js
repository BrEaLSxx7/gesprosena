(() => {
	'use strict';
	angular
		.module('GesPro')
		.controller('tipoproyectoController', tipoproyectoController);
	tipoproyectoController.$inject = ['$scope', 'DataBaseService', '$log', '$state', 'Upload', 'host', '$timeout', '$sessionStorage'];
	function tipoproyectoController($scope, DataBase, $log, $state, Upload, host, $timeout, $sessionStorage) {
		function ponerplantillahtml(plantilla) {
			return plantilla;
		}
		$scope.gestionrol = true;
		$scope.crud = ['Desactivar', 'Modificar', 'Eliminar'];
		if (typeof ($sessionStorage.date) !== "undefined") {
			$scope.date = $sessionStorage.date;
			DataBase.validaterol($scope.date.rol_id)
				.then((response) => {
					if (response.data.rol) {
						$scope.gestionrol = response.data.rol;
					} else {
						toastr.error('No tiene permisos para realizar esta acción');
						$state.go('dashboard');
					}
				})
				.catch((error) => {
					$log.error(error);
				});
			$scope.butonenabled = !false;
			$scope.butontype = (type) => {
				$scope.id = $sessionStorage.idtipo;
				if (type === 'Modificar') {
					$state.go('modificartipo');
				} else {
					DataBase.typegestion(type, $scope.id)
						.then((response) => {
							if (response.data.respuesta) {
								toastr.success(response.data.mensaje);
								$scope.datos = [];
								$scope.date.tipoproyecto.forEach((e, i) => {
									$scope.datos.push({ total: e.total, id: e.id });
									$log.log(e.total);
								});
								$scope.temp = response.data.data;
								$scope.temp.map((e)=>{
									for (var i = 0; i < $scope.datos.length; i++) {
										if (e.id === $scope.datos[i].id) {
											e.total = $scope.datos[i].total;
									}
									}
								});
								$scope.date.tipoproyecto = $scope.temp;
							} else {
								toastr.error(response.data.mensaje);
							}
						})
						.catch((error) => {
							$log.log(error);
						});
				}
			};
			$scope.gestipo = ((tipo) => {
				$scope.butonenabled = false;
				$sessionStorage.idtipo = tipo;
				DataBase.tipoproyecto(tipo)
					.then((response) => {
						$scope.plantilla = response.data[0];
						$scope.campos = response.data[1];
						$scope.plantilla.map((e, i) => {
							for (var j = 0; j < $scope.campos.length; j++) {
								if ($scope.plantilla[i].tipo_campo_id === $scope.campos[j].id) {
									e.nombre = $scope.campos[j].nombre;
								}
							}
						});
						$scope.tipodatos = ponerplantillahtml($scope.plantilla);
					})
					.catch((error) => {
						$log.error(error);
					});
			});

			$scope.name = $scope.date.nombre;
			$scope.correo = $scope.date.usuario;
			$scope.img = $scope.date.image + "?" + Date.now();
			$scope.upload = function (dataUrl, name) {
				Upload.upload({
					url: host + 'controller/uploadimg.controller.php',
					data: {
						file: Upload.dataUrltoBlob(dataUrl, name)
					},
				})
					.then((response) => {
						$scope.result = response.data;
						$scope.img = $scope.result.resultado;
						$scope.date.image = $scope.result.resultado;
						$scope.img = $scope.date.image + "?" + Date.now();
					})
					.catch(function (response) {
						if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
					});
			};
			$scope.logout = () => {
				delete $sessionStorage.date;
				$scope.out = true;
				DataBase.logout($scope.out)
					.then((response) => {
						if (response.data.respuesta) {
							toastr.success(response.data.mensaje);
							$state.go('inicio');
						}
					})
					.catch((error) => {
						$log.error(error);
					});
			};

		} else {
			$state.go('inicio');
		}
	}
})();