<?php
require_once '../model/login.model.php';
require_once '../view/json.php';
$data = array($_GET['email'], $_GET['password']);
$login = new LoginController();
$login->dateempty($data);
session_start();
$_SESSION['name'] = $login->result['usuario'];
$salidaJson = array('respuesta' => $login->mensajeOk, 'mensaje' => $login->mensajeError, 'resultado'=>$login->result);

$response = new Response($salidaJson);

echo $response->response();
// $login->mensajeError;
