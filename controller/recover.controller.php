<?php
require_once '../model/recover.model.php';
require_once '../view/json.php';

$correo = $_GET['recuperarcorreo'];
$recover= new recovercontroller();
$recover->recover($correo);
$salidaJson = array('respuesta' => $recover->mensajeOk, 'mensaje' => $recover->mensajeError);
$response = new Response($salidaJson);
echo $response->response();