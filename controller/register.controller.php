<?php
require_once '../model/register.model.php';
require_once '../view/json.php';
$perfil = $_POST['perfil'];
$rol = new Dashboardcontroller();
$rol->validaterol($perfil);
$rolid =$rol->rol_id;
$password = crypt($_POST['password'], password_hash($_POST['password'], 1));
$data = array('rolid' => $rolid, 'correo' => $_POST['correo'], 'tipodocumento' => $_POST['tipodocumento'], 'password' => $password, 'numerodocumento' => $_POST['numerodocumento'], 'nombre' => $_POST['nombre']);
$rol->ingresarusuario($data);
$salidaJson = array('respuesta' => $rol->mensajeOk, 'mensaje' => $rol->mensajeError);
$response = new Response($salidaJson);
echo $response->response();