<?php
require_once '../model/uploadimg.model.php';
require_once '../view/json.php';
session_start();
$name=$_SESSION['name'];
if ($_FILES ?? $_FILES['file']['tmp_name']) {
	if (move_uploaded_file($_FILES['file']['tmp_name'], "../imgprofile/" . basename($name.".png"))) {
		$upload= new Uploadimg();
		$url= "../imgprofile/" . basename($name . ".png");
		$upload->agregarimg($url, $name);
		$salidaJson = array('respuesta' => true, 'resultado' => $url);
		$response = new Response($salidaJson);
		echo $response->response();
	} else {
		echo "Ocurrió algún error al subir el fichero. No pudo guardarse.";
	}
}
