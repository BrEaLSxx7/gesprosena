<?php
require_once '../model/conection.php';

class Checkrol extends Conexion{
	public $perfil;
	private function valrol(){
		$conexion=$this->conexion();
		$stm=$conexion->prepare("SELECT id FROM rol WHERE nombre=:perfil");
		$stm->execute(array(':perfil'=> 'Coordinador'));
		$resultado=$stm->fetchAll();
		return $resultado;
	}
	public function evaluaterol(){
		$resultado=$this->valrol();
		$this->perfil=$resultado;
	}
}