<?php
require_once '../model/conection.php';
require_once '../model/login.model.php';
class Tipogestion extends LoginController
{
    public $mensaje="El sistema no se encuentra disponible";
    public $mensajeok=false;
    public $data;
    private function update($id)
    {
            $conexion=$this->conexion();
            $stm=$conexion->prepare("UPDATE tipo_proyecto set disabled_at=now() WHERE id=:id");
            $stm->execute(array(':id'=>$id));
            $resultado=$stm->fetchAll();
            return $resultado;
    }
    private function delete($id)
    {
        $conexion=$this->conexion();
            $stm=$conexion->prepare("UPDATE tipo_proyecto set delete_at WHERE id=:id");
            $stm->execute(array(':id'=>$id));
            $resultado=$stm->fetchAll();
            return $resultado;
    }
    public function checkgestion($type, $id)
    {
        if ($type=="Eliminar") {
            $resultado=$this->delete($id);
            $type="Eliminado";
        } elseif ($type=="Desactivar") {
            $resultado=$this->update($id);
            $type="Desactivado";
        }
        $this->data=$this->tipoproyecto();
        $this->mensaje=$type . " Correctamente";
        $this->mensajeok=true;
    }
}
