<?php
require_once '../model/conection.php';

class LoginController extends Conexion
{
    public $mensajeError = "El sistema no se encuentra disponible";
    public $mensajeOk = false;
    public $result;
    private function validate($data)
    {
        $conexion = $this->conexion();
        $stm = $conexion->prepare("SELECT usuario,contrasena,nombre,rol_id, url_image FROM usuario WHERE usuario = :usuario");
        $stm->execute(array(':usuario' => $data[0]));
        $resultado = $stm->fetchAll();
        return $resultado;
    }
    public function tipoproyecto()
    {
        $conexion = $this->conexion();
        $stm =$conexion->prepare("SELECT id,nombre FROM tipo_proyecto WHERE disabled_at is null AND deleted_at is null");
        $stm->execute();
        $resultado = $stm->fetchAll();
        return $resultado;
    }
    private function proyecto()
    {
         $conexion = $this->conexion();
        $stm =$conexion->prepare("SELECT tipo_proyecto_id,usuario_id_responsable,estado_id,nombre,created_at FROM proyecto");
        $stm->execute();
        $resultado = $stm->fetchAll();
        return $resultado;
    }
    public function dateempty($data)
    {
        if ($data[0] != '' && $data[1] != '') {
            $resultado = $this->validate($data);
            if (count($resultado) > 0) {
                $pass= $resultado[0]->contrasena;
                // var_dump(password_verify($data[1], $pass));
                if (password_verify($data[1], $pass)) {
                    $rol_id=crypt($resultado[0]->rol_id, password_hash($resultado[0]->rol_id, 1));
                    if ($resultado[0]->rol_id=1) {
                        $resulttipoproyecto=$this->tipoproyecto();
                        $resultadoproyecto=$this->proyecto();
                        $this->result = array('usuario' => $resultado[0]->usuario, 'nombre' => $resultado[0]->nombre, 'rol_id' => $rol_id, 'image'=> $resultado[0]->url_image, 'tipoproyecto'=>$resulttipoproyecto, 'proyectos'=>$resultadoproyecto);
                    } else {
                        $resultadoproyecto = $this->proyecto();
                        $this->result = array('usuario' => $resultado[0]->usuario, 'nombre' => $resultado[0]->nombre, 'rol_id' => $rol_id, 'image'=> $resultado[0]->url_image, 'proyectos' => $resultadoproyecto);
                    }
                    $this->mensajeOk = true;
                    $this->mensajeError = 'Logueado Correctamente';
                } else {
                    $this->mensajeError = 'Email y/o Contraseña incorrectos';
                }
            } else {
                $this->mensajeError = 'Email y/o Contraseña incorrectos';
            }
        } else {
            $this->mensajeError = 'Todos los campos son requeridos';
        }
    }
}
