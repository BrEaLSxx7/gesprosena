<?php
require_once '../model/conection.php';

class recovercontroller extends Conexion
{

	public $mensajeError = "El sistema no se encuentra disponible";
	public $mensajeOk = false;
	public $result;
	private function validate($correo)
	{
		$conexion = $this->Conexion();
		$stm = $conexion->prepare("SELECT usuario,contrasena,nombre,rol_id FROM usuario WHERE usuario = :usuario");
		$stm->execute(array(':usuario' => $correo));
		$resultado = $stm->fetchAll();
		return $resultado;
	}

	public function recover($correo)
	{
		$resultado = $this->validate($correo);
		if (count($resultado) > 0) {
			$this->mensajeError = "Ya fue envíado un correo de restauración";
			$this->mensajeOk = true;
		}
		else {
			$this->mensajeError = 'El Email no se encuentra registrado';
		}
	}
}