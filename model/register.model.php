<?php
require_once '../model/conection.php';

class Dashboardcontroller extends Conexion
{
    public $mensajeError = "El sistema no se encuentra disponible";
    public $mensajeOk = false;
    public $rol_id;
    private function conectrol($perfil)
    {
        $conexion = $this->Conexion();
        $stm = $conexion->prepare("SELECT id, nombre FROM rol WHERE nombre = :perfil");
        $stm->execute(array(':perfil' => $perfil));
        $resultado = $stm->fetchAll();
        return $resultado;
    }
    private function agregarusuario($data)
    {
        $conexion = $this->Conexion();
        $stm = $conexion->prepare("SELECT usuario FROM usuario WHERE usuario = :correo");
        $stm->execute(array(':correo' => $data['correo']));
        $resultado = $stm->fetchAll();
        return $resultado;
    }
    private function validatenumero($data)
    {
        $conexion = $this->Conexion();
        $stm = $conexion->prepare("SELECT numero_documento FROM usuario WHERE numero_documento = :numero");
        $stm->execute(array(':numero' => $data['numerodocumento']));
        $resultado = $stm->fetchAll();
        return $resultado;
    }
    private function ingresausuario($data)
    {
        $conexion = $this->Conexion();
        $stm=$conexion->prepare("INSERT INTO usuario (nombre, usuario, tipo_documento, numero_documento, rol_id, contrasena) VALUES (:nombre, :correo, :tipo, :numero, :rol, :contrasena)");
        $stm->execute(array(':nombre' => $data['nombre'], ':correo'=>$data['correo'], ':tipo'=>$data['tipodocumento'], ':numero'=>$data['numerodocumento'], ':rol'=>$data['rolid'], ':contrasena'=>$data['password']));
    }
    public function validaterol($perfil)
    {
        $result = $this->conectrol($perfil);
        $this->rol_id = $result[0]->id;
    }
    public function ingresarusuario($data)
    {
        $resultado=$this->agregarusuario($data);
        if (count($resultado)==0) {
            $resnumero=$this->validatenumero($data);
            if (count($resnumero)==0) {
                $this->mensajeError = 'Registrado correctamente';
                $this->mensajeOk = true;
                $this->ingresausuario($data);
            } else {
                $this->mensajeOk = false;
                $this->mensajeError = 'El numero de documento ingresado ya se encuentra registrado';
            }
        } else {
            $this->mensajeOk = false;
            $this->mensajeError='El correo ingresado ya se encuentra registrado';
        }
    }
}
