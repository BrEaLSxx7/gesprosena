<?php
require_once '../model/conection.php';

class tipo extends Conexion
{
    public $type;
    private function plantilla($tipo)
    {
        $conexion=$this->conexion();
        $stm=$conexion->prepare("SELECT tipo_campo_id, posicion,valor FROM plantilla WHERE tipo_proyecto_id=:tipo ORDER BY posicion");
        $stm->execute(array(':tipo'=>$tipo));
        $resultado=$stm->fetchAll();
        return $resultado;
    }
    private function tipocampo()
    {
        $conexion=$this->conexion();
        $stm=$conexion->prepare("SELECT id,nombre FROM tipo_campo");
        $stm->execute();
        $resultado=$stm->fetchAll();
        return $resultado;
    }
    public function tipopro($tipo)
    {
        $plantilla=$this->plantilla($tipo);
				$tipocampo=$this->tipocampo();
				$this->type=(array($plantilla,$tipocampo));
    }
}
